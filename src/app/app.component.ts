import {Component} from '@angular/core';

import {Platform, NavController} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {Pages} from './interfaces/pages';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    public appPages: Array<Pages>;

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        public navCtrl: NavController
    ) {
        this.appPages = [
            {
                title: 'Home',
                url: '/home-results',
                direct: 'root',
                icon: 'home'
            },
            {
                title: 'About',
                url: '/about',
                direct: 'forward',
                icon: 'information-circle-outline'
            },
            {
                title: 'App Settings',
                url: '/settings',
                direct: 'forward',
                icon: 'cog'
            }
        ];

        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            this.initPointzi();
        }).catch(() => {});
    }

    goToEditProgile() {
        this.navCtrl.navigateForward('edit-profile');
    }

    initPointzi() {
        const APP_KEY = 'PAQACORE';
        const EVENT_KEY = 'evtbkjg5ejgAHJC65F23x3fxUmHlpdHxo';

        window['pointzi'].load(true).then(function () {
            const value = window.localStorage.getItem('CUID');
            if (value !== '' && value !== null && value !== undefined) {
                window['pointzi'].register(APP_KEY, EVENT_KEY, value);
            } else {
                navigator['notification'].prompt(
                    'Please enter CUID',  // message
                    (results) => {
                        if (results.buttonIndex === 1) {
                            window.localStorage.setItem('CUID', results.input1);
                            window['pointzi'].register(APP_KEY, EVENT_KEY, results.input1);
                        }
                    },                  // callback to invoke
                    'CUID',            // title
                    ['Ok', 'Exit'],             // buttonLabels
                    'Jane Doe'                 // defaultText
                );
            }
        });
    }

    logout() {
        this.navCtrl.navigateRoot('/');
    }
}
